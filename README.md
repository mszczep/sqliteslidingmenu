# SQLiteSlidingMenu

A simple to-do list, allows you to add short tasks and remove them. 

Starting screen

![Screenshot 1](img/ss1.png)

Adding a new task

![Screenshot 2](img/ss2.png)

Removing tasks

![Screenshot 3](img/ss3.png)

Done

![Screenshot 4](img/ss4.png)