package com.example.michal.sqliteslidingmenu;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import com.example.michal.sqliteslidingmenu.db.DBadapter;
import com.example.michal.sqliteslidingmenu.model.listviewModel;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements OnClickListener {

    //slidingmenu
    ImageButton button_menu;
    LinearLayout SQLcontent, slidingmenu;
    LinearLayout.LayoutParams contentParams;
    TranslateAnimation slide;
    int margin, animateFrom , animateTo = 0;
    boolean menuOpen = false;

    //db
    private ListView listview;
    private Button btnAdd,btnCancel, btnSave, btnRemove;
    private EditText bEditText;
    private DBadapter dbadapter;
    private Cursor cursor;
    private List<listviewModel> listContent;
    private listviewAdapter listAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();
        initListView();
        initButtonListener();

    }

    private void initUI(){
        slidingmenu =  findViewById(R.id.slidingmenu);
        SQLcontent =  findViewById(R.id.SQLcontent);
        contentParams = (LinearLayout.LayoutParams)SQLcontent.getLayoutParams();
        contentParams.width = getWindowManager().getDefaultDisplay().getWidth();
        contentParams.leftMargin = -(slidingmenu.getLayoutParams().width);
        SQLcontent.setLayoutParams(contentParams);
        button_menu = findViewById(R.id.button_menu);
        button_menu.setOnClickListener(this);

        btnAdd =  findViewById(R.id.buttonAdd);
        btnCancel =  findViewById(R.id.buttonCancel);
        btnRemove = findViewById(R.id.buttonRemove);
        btnSave =  findViewById(R.id.buttonSave);
        bEditText =  findViewById(R.id.editTextNewTask);
        listview =  findViewById(R.id.SQLlistview);


    }

    private void initListView(){
        dbFillListView();
        initListViewClicks();
    }

    private void dbFillListView(){
        dbadapter = new DBadapter(getApplicationContext());
        dbadapter.open();
        getContent();
        listAdapter = new listviewAdapter(this,listContent);
        listview.setAdapter(listAdapter);

    }

    private void getContent(){
    listContent = new ArrayList<listviewModel>();
    cursor = getAllEntriesFromDB();
    updateList();
    }

    private Cursor getAllEntriesFromDB(){
        cursor = dbadapter.getAllContent();
        if(cursor!=null){
            startManagingCursor(cursor);
            cursor.moveToFirst();
        }
        return cursor;
    }

    private void updateList(){
        if(cursor != null && cursor.moveToFirst()){
            do {
                long id = cursor.getLong(DBadapter.ID_COLUMN);
                String description = cursor.getString(DBadapter.DESCRIPTION_COLUMN);
                boolean finished = cursor.getInt(DBadapter.FINISHED_COLUMN) >0;
                listContent.add(new listviewModel(id,description,finished));
            } while(cursor.moveToNext());
        }
    }

    @Override
    protected void onDestroy(){
        if(dbadapter !=null)
            dbadapter.close();
        super.onDestroy();
    }


    private void initListViewClicks(){
    listview.setOnItemClickListener(new OnItemClickListener(){
    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id){
        listviewModel model = listContent.get(position);
        if(model.checkIfFinished()){
            dbadapter.updateList(model.getId(),model.getDescription(),false);
        }else{
            dbadapter.updateList(model.getId(),model.getDescription(),true);
        }
            updateListViewData();
         }
            });
    }


    private void updateListViewData(){
        cursor.requery();
        listContent.clear();
        updateList();
        listAdapter.notifyDataSetChanged();
    }


    private void initButtonListener(){
        OnClickListener onClickListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
            switch(view.getId()){
                case R.id.buttonAdd:
                    addNewTask();
                    break;
                case R.id.buttonRemove:
                    clearCompletedTasks();
                    break;
                case R.id.buttonCancel:
                    cancelTask();
                    break;
                case R.id.buttonSave:
                    saveNewTask();
                    break;

                default:
                    break;
            }
            }
        };
        btnAdd.setOnClickListener(onClickListener);
        btnRemove.setOnClickListener(onClickListener);
        btnCancel.setOnClickListener(onClickListener);
        btnSave.setOnClickListener(onClickListener);
    }

    private void showOnlyNewTaskPanel(){

        setVisibilityOf(btnAdd,false);
        setVisibilityOf(btnRemove,false);
        setVisibilityOf(btnSave,true);
        setVisibilityOf(btnCancel,true);
        setVisibilityOf(bEditText,true);

    }

    private void showOnlyControlPanel(){
        setVisibilityOf(btnAdd,true);
        setVisibilityOf(btnRemove,true);
        setVisibilityOf(btnSave,false);
        setVisibilityOf(btnCancel,false);
        setVisibilityOf(bEditText,false);

    }

    private void setVisibilityOf(View v, boolean visible){
        int visibility = visible ? View.VISIBLE : View.GONE;
        v.setVisibility(visibility);
    }

    private void hideKeyboard(){
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(bEditText.getWindowToken(),0);
    }

    private void addNewTask(){
        showOnlyNewTaskPanel();
    }

    private void saveNewTask(){
        String description = bEditText.getText().toString();
        if(description.equals("")){
            bEditText.setError("Description cant be empty");
        }
        else{
            dbadapter.insertContent(description);
            bEditText.setText("");
            hideKeyboard();
            showOnlyControlPanel();
        }
        updateListViewData();
    }

    private void cancelTask(){
        bEditText.setText("");
        showOnlyControlPanel();
    }

    private void clearCompletedTasks(){
        if(cursor != null && cursor.moveToFirst()){
            do{
                if(cursor.getInt(dbadapter.FINISHED_COLUMN)==1){
                    long id = cursor.getLong(dbadapter.ID_COLUMN);
                    dbadapter.deleteContent(id);
                }
            }
            while (cursor.moveToNext());
        }
        updateListViewData();
    }



    public boolean onKeyDown(int keyCode, KeyEvent keyEvent)
    {
        if(keyCode==KeyEvent.KEYCODE_BACK)
        {
            if(menuOpen)
            {
                slideMenuIn(0, -(slidingmenu.getLayoutParams().width), -(slidingmenu.getLayoutParams().width));
                menuOpen=false;
                return true;
            }
        }
        return super.onKeyDown(keyCode,keyEvent);
    }

public void onClick(View v)
{

    if(contentParams.leftMargin == -(slidingmenu.getLayoutParams().width))
    {
    animateFrom = 0;
    animateTo = (slidingmenu.getLayoutParams().width);
    margin = 0;
    menuOpen = true;
    }
    else
    {
        animateFrom = 0;
        animateTo = -(slidingmenu.getLayoutParams().width);
        margin = -(slidingmenu.getLayoutParams().width);
        menuOpen=false;
    }
slideMenuIn(animateFrom,animateTo,margin);

}

public void slideMenuIn(int animateFrom, int animateTo, final int margin)
{
    slide = new TranslateAnimation(animateFrom, animateTo, 0, 0);
    slide.setDuration(300);
    slide.setFillEnabled(true);
    slide.setAnimationListener(new AnimationListener()
    {
        public void onAnimationEnd(Animation animation)
        {
            contentParams.setMargins(margin,0,0,0);
            SQLcontent.setLayoutParams(contentParams);
        }
        public void onAnimationRepeat(Animation animation){}
        public void onAnimationStart(Animation animation){}
    });
SQLcontent.startAnimation(slide);
}

}
