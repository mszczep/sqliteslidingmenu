package com.example.michal.sqliteslidingmenu.model;

public class listviewModel {
    private long id;
    private String description;
    private boolean finished;
    public listviewModel(long id, String description, boolean finished)
    {
        this.id=id;
        this.description=description;
        this.finished=finished;
    }
    public long getId(){return id;}
    public void setId(long id){this.id=id;}
    public String getDescription(){return description;}
    public void setDescription(String description){this.description=description;}
    public boolean checkIfFinished(){return finished;}
    public void setFinished(boolean finished){this.finished=finished;}


}
