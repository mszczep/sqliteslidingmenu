package com.example.michal.sqliteslidingmenu.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;

import com.example.michal.sqliteslidingmenu.model.listviewModel;

public class DBadapter {

    private static final String DEBUG_TAG = "SqLiteListManager";
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "database.db";
    private static final String DB_LIST_TABLE = "list";
    public static final String KEY_ID = "_id";
    public static final String ID_OPTIONS = "INTEGER PRIMARY KEY AUTOINCREMENT";
    public static final int ID_COLUMN = 0;
    public static final String KEY_DESCRIPTION = "description";
    public static final String DESCRIPTION_OPTIONS = "TEXT NOT NULL";
    public static final int DESCRIPTION_COLUMN = 1;
    public static final String KEY_FINISHED = "finished";
    public static final String FINISHED_OPTIONS = "INTEGER DEFAULT 0";
    public static final int FINISHED_COLUMN = 2;
    private static final String DB_CREATE_LIST_TABLE =
            "CREATE TABLE " + DB_LIST_TABLE + "( " +
                    KEY_ID + " " + ID_OPTIONS + ", " +
                    KEY_DESCRIPTION + " " + DESCRIPTION_OPTIONS + ", " +
                    KEY_FINISHED + " " + FINISHED_OPTIONS +
                    ");";
    private static final String DROP_LIST_TABLE =
            "DROP TABLE IF EXISTS " + DB_LIST_TABLE;

    private SQLiteDatabase db;
    private Context context;
    private DatabaseHelper dbHelper;

private static class DatabaseHelper extends SQLiteOpenHelper {
    public DatabaseHelper(Context context, String name, CursorFactory factory, int version)
    {
        super(context,name,factory,version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    db.execSQL(DB_CREATE_LIST_TABLE);
    Log.d(DEBUG_TAG,"Database creating...");
    Log.d(DEBUG_TAG,"Table" +DB_LIST_TABLE+"ver."+DB_VERSION+"created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL(DROP_LIST_TABLE);
        Log.d(DEBUG_TAG, "Database updating...");
        Log.d(DEBUG_TAG, "Table" + DB_LIST_TABLE + "updated from ver."+oldVersion+"to ver."+newVersion);
        Log.d(DEBUG_TAG,"All data is lost.");
        onCreate(db);
    }
}

public DBadapter (Context context) {this.context= context;}
public DBadapter open(){
    dbHelper =new DatabaseHelper(context,DB_NAME,null,DB_VERSION);
    try{
        db = dbHelper.getWritableDatabase();
    } catch(SQLException e ){
        db = dbHelper.getReadableDatabase();
    }
    return this;
}

public void close(){dbHelper.close();}

public long insertContent(String description){
    ContentValues newContentValues = new ContentValues();
    newContentValues.put(KEY_DESCRIPTION,description);
    return db.insert(DB_LIST_TABLE,null,newContentValues);
}
public boolean updateList(listviewModel content){
    long id = content.getId();
    String description = content.getDescription();
    boolean finished =content.checkIfFinished();
    return updateList(id,description,finished);
}

    public boolean updateList(long id, String description, boolean finished) {
    String where = KEY_ID+"="+id;
    int finishedTask = finished ? 1:0;
    ContentValues updateContentValues = new ContentValues();
    updateContentValues.put(KEY_DESCRIPTION, description);
    updateContentValues.put(KEY_FINISHED, finishedTask);
    return db.update(DB_LIST_TABLE,updateContentValues,where,null)>0;
    }

public boolean deleteContent(long id){
    String where = KEY_ID+"="+id;
    return db.delete(DB_LIST_TABLE,where,null)>0;
}

public Cursor getAllContent(){
    String[] columns = {KEY_ID,KEY_DESCRIPTION,KEY_FINISHED};
    return db.query(DB_LIST_TABLE,columns,null,null,null,null,null);
}

public listviewModel getContent (long id){
    String[] columns ={KEY_ID,KEY_DESCRIPTION,KEY_FINISHED};
    String where = KEY_ID+"="+id;
    Cursor cursor =db.query(DB_LIST_TABLE,columns,where,null,null,null,null);
    listviewModel content =null;
    if(cursor !=null && cursor.moveToFirst()){
        String description = cursor.getString(DESCRIPTION_COLUMN);
        boolean finished = cursor.getInt(FINISHED_COLUMN) > 0 ? true : false;
        content = new listviewModel(id, description,finished);
    }
    return content;
}
}
