package com.example.michal.sqliteslidingmenu;

import android.app.Activity;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.michal.sqliteslidingmenu.model.listviewModel;

import java.util.List;

public class listviewAdapter extends ArrayAdapter<listviewModel> {
    private Activity context;
    private List<listviewModel> content;

    public listviewAdapter(Activity context, List<listviewModel> content)
    {
        super(context,R.layout.listview_item, content);
        this.context=context;
        this.content=content;
    }

    static class ViewHolder {
        public TextView mContentDescription;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        ViewHolder viewHolder;
        View rowView = convertView;
        if(rowView==null){
            LayoutInflater layoutinflater =context.getLayoutInflater();
            rowView = layoutinflater.inflate(R.layout.listview_item,null,true);
            viewHolder = new ViewHolder();
            viewHolder.mContentDescription = (TextView) rowView.findViewById(R.id.label);
            rowView.setTag(viewHolder);
        }
        else{
            viewHolder =(ViewHolder)rowView.getTag();
        }
        listviewModel Content = content.get(position);
        viewHolder.mContentDescription.setText(Content.getDescription());
        if (Content.checkIfFinished()) {
        viewHolder.mContentDescription.setPaintFlags(viewHolder.mContentDescription.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);
        }
        else{
        viewHolder.mContentDescription.setPaintFlags(viewHolder.mContentDescription.getPaintFlags()&~Paint.STRIKE_THRU_TEXT_FLAG);
        }
        return rowView;
    }
}
